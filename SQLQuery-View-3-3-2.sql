﻿--3.3 Creati urmatoarele vederi:
-- 1a 	 View Proiecte: nume, domeniul, nume managerului, nr angajaţi pe proiect, faza finală a proiectului. 
-- 1b ✓	 Sa se afiseze si proiectele care nu au inca alocata o echipa.
-- 2  ✓	 View pt numele proiectelor care au echipele cu numar maxim de membri.
-- 3  ✓ View pt Filiale: nume, adresa, directorul, nr birouri din fiecare, nr angajati. ✓
-- 4  ✓ View pt Angajati:nume, varsta in ani.
-- use:	View Designer + instructiunea Create View.

use Intreprinderi01;

Create View View3_3_1a
AS
SELECT  
from Proiecte, FazeleProiectelor, Faze
WHERE 
GO

Create View View3_3_1b
AS
SELECT numeP as Proiecte_viitoare_fara_echipa
from Proiecte p
WHERE NOT EXISTS (SELECT IDP FROM AngajatiPeProiecte  a WHERE a.IDP=p.IDP);
GO

--SUBSTRING(string, start, length)

Create View View3_3_4
AS
SELECT nume + ' ' + prenume AS Angajat, (YEAR(GETDATE())- YEAR(convert(date, substring(CNP,2,6),12))) AS Varsta
from Angajati
GO

DROP VIEW View3_3_4;

SELECT [Angajat], [Varsta]
  FROM [Intreprinderi01].[dbo].[View3_3_4]

SELECT * FROM Filiale;
SELECT * FROM Proiecte;
SELECT * FROM Birouri;
SELECT * FROM Proiecte;
SELECT * FROM Angajati;
SELECT * FROM AngajatiPeProiecte ORDER BY IDP;
SELECT * FROM FazeleProiectelor;
SELECT * FROM Faze;

UPDATE  Angajati 
SET CNP='1880519135430' 
	WHERE IDA=14;

INSERT INTO Angajati VALUES 
(16, 'Cocalaru', 'Aurel', 'Cluj-Napoca, str. Mehedinti nr. 18/3', '2017-11-01', '1950211125569', 22, 'programator'),
(17, 'Milos', 'Ramona', 'Brasov, B-dul. 15 Noiembrie nr. 76/33', '2017-11-01', '2920518260099', 26, 'programator'),
(18, 'Milos', 'Ricardo', 'Brasov, B-dul. 15 Noiembrie nr. 76/33', '2017-11-01', '1890309100369', 26, 'programator'),
(19, 'Baggins', 'Elon', 'Cluj-Napoca, str. Vanatorului nr. 11', '2017-11-01', '1760401120084', 22, 'programator'),
(20, 'Shrek', 'Fiona', 'Cluj-Napoca, str. Meteor nr. 3', '2017-11-01', '2900824125603', 22, 'programator');

use Intreprinderi01;
Create View View3_3_2
AS
SELECT numeP AS Proiect,
	(SELECT MAX(nr_angajati.nr)
                 FROM   (SELECT COUNT(a.IDA) AS nr 
				 FROM AngajatiPeProiecte AS a, Proiecte p
				 WHERE a.IDP=p.IDP) as nr_angajati) AS NrAngajati
	FROM Proiecte
	INNER JOIN AngajatiPeProiecte ON Proiecte.IDP = AngajatiPeProiecte.IDP
GO

SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'Angajati' AND 
     COLUMN_NAME = 'CNP'




	 use Intreprinderi01;
	 sp_dbfixedrolepermission @rolename='db_accessadmin';