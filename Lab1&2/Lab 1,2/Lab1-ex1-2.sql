-- Lab.1. Ex.1.2. 

SELECT nume, prenume, dataAngajarii, Filiale.numeF, Filiale.adresa 
FROM Angajati
JOIN Birouri ON (Angajati.IDB=Birouri.IDB)
JOIN Filiale ON (Birouri.IDF=Filiale.IDF);

ALTER SCHEMA dbo 
    TRANSFER Intreprinderi01.Filiale;

use Intreprinderi01
SELECT * FROM Angajati;

SELECT * FROM Birouri;

SELECT * FROM Filiale;


SELECT numeF, adresa, nrBirouri FROM Filiale;


INSERT INTO Angajati VALUES (11, 'Popescu', 'Ion', 'Cluj-Napoca, strada Padurii nr. 8', '2019-10-10', '1700807135743', 22);

-- Lab.1. Ex.1.3. 

EXECUTE AS LOGIN = 'Login1';
SELECT * FROM Angajati;
SELECT * FROM Birouri;
SELECT * FROM Filiale;

SELECT nume, prenume, dataAngajarii, Filiale.numeF, Filiale.adresa 
FROM Angajati
JOIN Birouri ON (Angajati.IDB=Birouri.IDB)
JOIN Filiale ON (Birouri.IDF=Filiale.IDF);

EXECUTE AS LOGIN = 'Login1';
INSERT INTO Angajati VALUES (15, 'Chira', 'Vlad', 'Bucuresti, Str. Plevnei nr. 19', '2008-02-01', '1701207100743', 24, 'manager de proiect');

use Intreprinderi01;
GRANT SELECT ON dbo.Angajati TO user2;
GRANT SELECT ON dbo.AngajatiPeProiecte TO user2;
GRANT SELECT ON dbo.Filiale TO user2;
GRANT SELECT ON dbo.Birouri TO user2;
GRANT SELECT ON dbo.Domenii TO user2;
GRANT SELECT ON dbo.Proiecte TO user2;
GRANT SELECT ON dbo.ProiectePeDomenii TO user2;

EXECUTE AS LOGIN = 'Login2';
SELECT * FROM Angajati; 
SELECT * FROM Birouri;
SELECT * FROM Filiale;

SELECT nume, prenume, dataAngajarii, Filiale.numeF, Filiale.adresa 
FROM Angajati
JOIN Birouri ON (Angajati.IDB=Birouri.IDB)
JOIN Filiale ON (Birouri.IDF=Filiale.IDF);

EXECUTE AS LOGIN = 'Login2';
INSERT INTO Angajati VALUES (16, 'Popescu', 'Georgiana', 'Cluj-Napoca, strada Padurii nr. 8', '2011-05-03', '2790621135743', 22, 'manager de proiect');

