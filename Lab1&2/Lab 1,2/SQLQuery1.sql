Create DATABASE Intreprinderi01;

DROP DATABASE Intreprinderi01;

create  table Filiale (
	IDF int NOT NULL UNIQUE,
	PRIMARY KEY (IDF),
	numeF varchar(50),
	oras varchar(30),
	adresa varchar(60),
	nrBirouri int
);

DROP TABLE Intreprinderi01.Filiale;
DROP TABLE Angajati;
DROP TABLE Birouri;
DROP TABLE Proiecte;

create  table Birouri (
	IDB int NOT NULL UNIQUE,
	PRIMARY KEY (IDB),
	IDF int,
	FOREIGN KEY (IDF) REFERENCES Filiale(IDF),
	capacitate int
);

create  table Angajati (
	IDA int NOT NULL,
	PRIMARY KEY (IDA),
	nume varchar(25),
	prenume varchar(25),
	adresa varchar(100),
	dataAngajarii date,
	CNP varchar(13),
	IDB int,
	FOREIGN KEY (IDB) REFERENCES Birouri(IDB)
);

create  table Proiecte (
	IDP int NOT NULL,
	PRIMARY KEY (IDP),
	IDA int,
	FOREIGN KEY (IDA) REFERENCES Angajati(IDA),
	capacitate int
);

SELECT * FROM Angajati;

SELECT * FROM Angajati;

INSERT INTO Angajati VALUES 
(12, 'Barsan', 'Mara Andreea', 'Bucuresti, Str. Mihai Eminescu nr. 48', '2017-11-01', '2890807115743', 25),
(13, 'Stefanescu', 'Liliana', 'Timisoara, Aleea Faget nr. 37', '2015-10-10', '2850802655743', 26),
(14, 'Bosca', 'Mihai Petru', 'Brasov, Str. Scortarilor nr. 12/77', '2018-10-01', '1888071357430', 24);

INSERT INTO Birouri VALUES 
(23, 'Cluj Capitala', 101, 40),
(24, 'Brasovcity', 103, 25),
(25, 'ABC', 102, 110),
(26, 'Happy Day', 104, 30);

INSERT INTO Filiale VALUES 
(102,'Capitala', 'Bucuresti', 'Bulevardul Decebal Nr. 7', 30),
(103,'Centru', 'Brasov', 'Calea Feldioarei Nr. 77', 12),
(104,'Vest', 'Timisoara', 'Calea Aradului Nr. 31', 17);