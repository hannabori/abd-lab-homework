﻿
-- CREATE LOGIN Login1 WITH PASSWORD = 'user1';

-- Lab.2. Ex.1.

ALTER TABLE Angajati 
ADD CONSTRAINT IDA
CHECK (IDA>0 AND IDA<=500);

ALTER TABLE Proiecte 
ADD CONSTRAINT IDP
CHECK (IDP>0 AND IDP<=500);

ALTER TABLE Domenii 
ADD CONSTRAINT IDD
CHECK (IDD>0 AND IDD<=500);

ALTER TABLE Birouri 
ADD CONSTRAINT IDB
CHECK (IDB>0 AND IDB<=500);

ALTER TABLE Filiale 
ADD CONSTRAINT IDF
CHECK (IDF>0 AND IDF<=500);

ALTER TABLE Angajati ADD
CONSTRAINT functieAngajat
 CHECK ((functieAngajat) IN ('director' , 'manager de proiect', 'programator', 'tester', 'intern'));

-- Lab.2. Ex.2.

ALTER TABLE Filiale 
ADD idAdminRetea int 
FOREIGN KEY (idAdminRetea) REFERENCES Angajati(IDA);

UPDATE  Filiale
SET idAdminRetea=13
WHERE IDF=104;

UPDATE  Filiale
SET idAdminRetea=14
WHERE IDF=103;

UPDATE  Filiale
SET idAdminRetea=11
WHERE IDF=101;

SELECT * FROM Filiale;
SELECT * FROM Angajati;

UPDATE  Angajati
SET functieAngajat='director'
WHERE IDA=13;

UPDATE  Angajati
SET functieAngajat='manager de proiect'
WHERE IDA=12;

UPDATE  Angajati
SET functieAngajat='tester'
WHERE IDA=11;

SELECT nume, prenume from Angajati 
JOIN Filiale ON Angajati.IDA=Filiale.idAdminRetea
WHERE Filiale.oras='Cluj-Napoca';

--Lista angajatilor care lucreaza atat la proiecte in domeniul „comert
-- electronic”, cat si la proiecte in domeniul „procesarii de imagini”. 

SELECT * FROM Domenii;
SELECT TOP 2 * FROM Angajati;
SELECT * FROM Proiecte;
SELECT * FROM ProiectePeDomenii;
SELECT * FROM AngajatiPeProiecte;

insert into Proiecte VALUES (401, 12,'Smart menu'), (402, 15,'Dating app');
insert into Proiecte VALUES (403, 15,'Laptop fix');

insert into ProiectePeDomenii VALUES (401, 1), (402, 3);
insert into ProiectePeDomenii VALUES (403, 1);

insert into AngajatiPeProiecte VALUES (403,13), (402, 15), (401,11), (401,14), (402,13);

use Intreprinderi01;

SELECT nume, prenume, d.denumire, p.numeP
from Angajati a, Domenii d, ProiectePeDomenii ppd, AngajatiPeProiecte app, Proiecte p
WHERE (a.IDA=app.IDA AND d.IDD=ppd.IDD AND p.IDP=ppd.IDP AND p.IDP=app.IDP) AND (d.denumire= 'comert electronic' OR d.denumire='procesare de imagini');


-- Lista angajatilor care lucreaza la cel mult 1 proiect (la 1 proiect saula nici unul) 

use Intreprinderi01;
SELECT nume, prenume 
from Angajati a
JOIN AngajatiPeProiecte app ON a.IDA=app.IDA
GROUP BY a.nume, a.prenume
HAVING	COUNT(app.IDP) < 2;

EXEC sp_change_users_login 'Report'; -- lists orphaned users for restored databases

-- Proiectele care au dead-line in luna noiembrie si explict, data limita

SELECT * FROM Proiecte;

ALTER TABLE Proiecte ADD Deadline date;

update Proiecte
set Deadline='11-11-2019'
where IDP='401';

update Proiecte
set Deadline='12-11-2019'
where IDP='402';

use Intreprinderi01;
update Proiecte
set Deadline='11-30-2019'
where IDP='403';

insert into Proiecte VALUES (404, 12,'MessChat','03-01-2020'), (405, 15,'Print','01-31-2020');

SELECT * FROM Proiecte
WHERE (month(Deadline)=11);

SELECT * FROM Proiecte
WHERE (month(Deadline)=11 AND day(Deadline)=30 AND year(Deadline)=2019);

-- Fazele fiecarui proiect ce se desfăşoară la filiala din Cluj-Napoca,
-- denumirile acestora si data limita (dead-line-ul)

SELECT p.numeP, p.Deadline, f.oras FROM Proiecte p, Angajati a, AngajatiPeProiecte app, Birouri b, Filiale f
WHERE p.IDP = app.IDP AND
app.IDA = a.IDA AND
a.IDB = b.IDB AND
b.IDF = f.IDF AND
f.oras='Cluj-Napoca';

CREATE TABLE Faze (
	IDFAZ int PRIMARY KEY,
	denumire varchar(30),
);

CREATE TABLE FazeleProiectelor (
	IDP int,
	FOREIGN KEY (IDP) REFERENCES Proiecte(IDP),
	IDFAZ int,
	FOREIGN KEY (IDFAZ) REFERENCES Faze(IDFAZ),
);

DROP TABLE Faze;

SELECT * FROM Birouri;
SELECT * FROM Proiecte;
SELECT * FROM Filiale;
SELECT * FROM Faze;

insert into Faze VALUES
(1, 'Initiation'),
(2, 'Planning'),
(3, 'Execution'),
(4, 'Monitoring and Control'),
(5, 'Closure');

insert into FazeleProiectelor VALUES 
(401, 1), (401, 2), (401, 3), (401, 4), (401, 5),
(402, 1), (402, 2), (402, 3), (402, 4), (402, 5),
(403, 1), (403, 3),
(404, 1), (404, 2), (404, 3), (404, 4),
(405, 3), (405, 4);

SELECT p.numeP, fa.denumire, p.Deadline, f.oras 
FROM Proiecte p, FazeleProiectelor fp, Faze fa, Angajati a, AngajatiPeProiecte app, Birouri b, Filiale f
WHERE p.IDP = app.IDP AND
p.IDP = fp.IDP AND
fp.IDFAZ = fa.IDFAZ AND
app.IDA = a.IDA AND
a.IDB = b.IDB AND
b.IDF = f.IDF AND
f.oras='Cluj-Napoca';

-- Denumirile proiectelor care au mai mult de 3 faze

SELECT p.numeP FROM Proiecte p 
JOIN FazeleProiectelor fp
ON p.IDP=fp.IDP 
GROUP BY p.numeP
HAVING COUNT(fp.IDFAZ)<3;

-- Lista angajatilor care au lucrat la proiecte finalizate in octombrie. INSERT INTO Proiecte VALUES (406, 12, 'Black Friday App','10-09-2019');SELECT * FROM Angajati;SELECT * FROM AngajatiPeProiecte;insert into AngajatiPeProiecte VALUES (406, 12), (406, 14), (406,11), (404,13),(405,14);SELECT a.nume, a.prenume -- ,a.ida, p.numeP, p.Deadline  FROM Angajati a, Proiecte p, AngajatiPeProiecte appWHERE p.IDP=app.IDP AND app.IDA=a.IDAAND month(p.Deadline)=10 AND year(p.Deadline)=2019;-- Beneficiarul pentru un anumit proiect, valoarea proiectului
-- si banca prin care se realizeaza tranzactii cu beneficiarul respectiv.ALTER TABLE Proiecte ADD client varchar(50);ALTER TABLE Proiecte ADD valoare_eur int;ALTER TABLE Proiecte ADD banca varchar(40);update Proiecte SET client='Hilton', valoare_eur=130000, banca='Unicredit'WHERE  IDP=401;update Proiecte SET client='WK SRL', valoare_eur=3000, banca='Erste'WHERE  IDP=406;SELECT numeP, client, valoare_eur, banca FROM Proiecte;/* CREATE TABLE Clients (	IDC int PRIMARY KEY,	nume varchar(40),	CUI_CNP varchar(13),	banca varchar(40),	IBAN varchar(24)); */