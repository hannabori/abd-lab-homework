Create DATABASE Intreprinderi01;

-- Create SCHEMA Intreprinderi01;

DROP DATABASE Intreprinderi01;

/* create  table Intreprinderi01.Filiale (
	IDF int NOT NULL UNIQUE,
	PRIMARY KEY (IDF),
	numeF varchar(50),
	oras varchar(30),
	adresa varchar(60),
	nrBirouri int
); */

DROP TABLE Filiale;
DROP TABLE Angajati;
DROP TABLE Birouri;
DROP TABLE Proiecte;
DROP TABLE Domenii;
DROP TABLE AngajatiPeProiecte;
DROP TABLE ProiectePeDomenii;

create  table Birouri (
	IDB int NOT NULL UNIQUE,
	PRIMARY KEY (IDB),
	numeB varchar(50),
	IDF int,
	FOREIGN KEY (IDF) REFERENCES Filiale(IDF),
	capacitate int
);

create  table Angajati (
	IDA int NOT NULL,
	PRIMARY KEY (IDA),
	nume varchar(25),
	prenume varchar(25),
	adresa varchar(100),
	dataAngajarii date,
	CNP varchar(13),
	IDB int,
	FOREIGN KEY (IDB) REFERENCES Birouri(IDB)
);

create  table Proiecte (
	IDP int NOT NULL,
	managerProiect int
	FOREIGN KEY (managerProiect) REFERENCES Angajati(IDA),
	PRIMARY KEY (IDP),
	numeP varchar(50) not null
);

Create table AngajatiPeProiecte(
	IDP int,
	FOREIGN KEY (IDP) REFERENCES Proiecte(IDP),
	IDA int,
	FOREIGN KEY (IDA) REFERENCES Angajati(IDA)
);

Create table Domenii(
	IDD int NOT NULL,
	PRIMARY KEY (IDD),
	denumire varchar(50)
);
	
Create table ProiectePeDomenii(
	IDP int,
	FOREIGN KEY (IDP) REFERENCES Proiecte(IDP),
	IDD int,
	FOREIGN KEY (IDD) REFERENCES Domenii(IDD)
);

SELECT * FROM Angajati;

SELECT * FROM Angajati;

INSERT INTO Angajati VALUES (11, 'Popescu', 'Ion', 'Cluj-Napoca, strada Padurii nr. 8', '2019-10-10', '1700807135743', 22);
INSERT INTO Birouri VALUES (22, );
INSERT INTO Filiale VALUES (101, 'FilialaUno', 'Cluj-Napoca', 'Str. Motilor Nr. 16', 6);

INSERT INTO Domenii VALUES (1, 'comert electronic'),
					(2, 'prelucrari de voce'), 
					(3, 'procesare de imagini'),
					(4, 'biomedicale'), 
					(5, 'web desing'), 
					(6, 'economic'), 
					(7, 'invatamant la distanta');