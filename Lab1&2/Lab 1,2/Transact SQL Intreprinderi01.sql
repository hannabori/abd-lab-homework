USE [master]
GO
/****** Object:  Database [Intreprinderi01]    Script Date: 11/1/2019 4:44:36 PM ******/
CREATE DATABASE [Intreprinderi01]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Intreprinderi01', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Intreprinderi01.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Intreprinderi01_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Intreprinderi01_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Intreprinderi01] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Intreprinderi01].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Intreprinderi01] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Intreprinderi01] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Intreprinderi01] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Intreprinderi01] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Intreprinderi01] SET ARITHABORT OFF 
GO
ALTER DATABASE [Intreprinderi01] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Intreprinderi01] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Intreprinderi01] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Intreprinderi01] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Intreprinderi01] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Intreprinderi01] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Intreprinderi01] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Intreprinderi01] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Intreprinderi01] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Intreprinderi01] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Intreprinderi01] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Intreprinderi01] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Intreprinderi01] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Intreprinderi01] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Intreprinderi01] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Intreprinderi01] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Intreprinderi01] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Intreprinderi01] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Intreprinderi01] SET  MULTI_USER 
GO
ALTER DATABASE [Intreprinderi01] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Intreprinderi01] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Intreprinderi01] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Intreprinderi01] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Intreprinderi01] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Intreprinderi01]
GO
/****** Object:  User [user2]    Script Date: 11/1/2019 4:44:36 PM ******/
CREATE USER [user2] FOR LOGIN [Login2] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [user1]    Script Date: 11/1/2019 4:44:36 PM ******/
CREATE USER [user1] FOR LOGIN [Login1] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Schema [Intreprinderi01]    Script Date: 11/1/2019 4:44:36 PM ******/
CREATE SCHEMA [Intreprinderi01]
GO
/****** Object:  Table [dbo].[Angajati]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Angajati](
	[IDA] [int] NOT NULL,
	[nume] [varchar](25) NULL,
	[prenume] [varchar](25) NULL,
	[adresa] [varchar](100) NULL,
	[dataAngajarii] [date] NULL,
	[CNP] [varchar](13) NULL,
	[IDB] [int] NULL,
	[functieAngajat] [varchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AngajatiPeProiecte]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AngajatiPeProiecte](
	[IDP] [int] NULL,
	[IDA] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Birouri]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Birouri](
	[IDB] [int] NOT NULL,
	[numeB] [varchar](50) NULL,
	[IDF] [int] NULL,
	[capacitate] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[IDB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Domenii]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Domenii](
	[IDD] [int] NOT NULL,
	[denumire] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Faze]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faze](
	[IDFAZ] [int] NOT NULL,
	[denumire] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDFAZ] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FazeleProiectelor]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FazeleProiectelor](
	[IDP] [int] NULL,
	[IDFAZ] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Filiale]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Filiale](
	[IDF] [int] NOT NULL,
	[numeF] [varchar](50) NULL,
	[oras] [varchar](30) NULL,
	[adresa] [varchar](60) NULL,
	[nrBirouri] [int] NULL,
	[idAdminRetea] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDF] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[IDF] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proiecte]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proiecte](
	[IDP] [int] NOT NULL,
	[managerProiect] [int] NULL,
	[numeP] [varchar](50) NOT NULL,
	[Deadline] [date] NULL,
	[client] [varchar](50) NULL,
	[valoare_eur] [int] NULL,
	[banca] [varchar](40) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProiectePeDomenii]    Script Date: 11/1/2019 4:44:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProiectePeDomenii](
	[IDP] [int] NULL,
	[IDD] [int] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Angajati]  WITH CHECK ADD FOREIGN KEY([IDB])
REFERENCES [dbo].[Birouri] ([IDB])
GO
ALTER TABLE [dbo].[AngajatiPeProiecte]  WITH CHECK ADD FOREIGN KEY([IDA])
REFERENCES [dbo].[Angajati] ([IDA])
GO
ALTER TABLE [dbo].[AngajatiPeProiecte]  WITH CHECK ADD FOREIGN KEY([IDP])
REFERENCES [dbo].[Proiecte] ([IDP])
GO
ALTER TABLE [dbo].[Birouri]  WITH CHECK ADD FOREIGN KEY([IDF])
REFERENCES [dbo].[Filiale] ([IDF])
GO
ALTER TABLE [dbo].[FazeleProiectelor]  WITH CHECK ADD FOREIGN KEY([IDFAZ])
REFERENCES [dbo].[Faze] ([IDFAZ])
GO
ALTER TABLE [dbo].[FazeleProiectelor]  WITH CHECK ADD FOREIGN KEY([IDP])
REFERENCES [dbo].[Proiecte] ([IDP])
GO
ALTER TABLE [dbo].[Filiale]  WITH CHECK ADD FOREIGN KEY([idAdminRetea])
REFERENCES [dbo].[Angajati] ([IDA])
GO
ALTER TABLE [dbo].[Proiecte]  WITH CHECK ADD FOREIGN KEY([managerProiect])
REFERENCES [dbo].[Angajati] ([IDA])
GO
ALTER TABLE [dbo].[ProiectePeDomenii]  WITH CHECK ADD FOREIGN KEY([IDD])
REFERENCES [dbo].[Domenii] ([IDD])
GO
ALTER TABLE [dbo].[ProiectePeDomenii]  WITH CHECK ADD FOREIGN KEY([IDP])
REFERENCES [dbo].[Proiecte] ([IDP])
GO
ALTER TABLE [dbo].[Angajati]  WITH CHECK ADD  CONSTRAINT [functieAngajat] CHECK  (([functieAngajat]='Sunday' OR [functieAngajat]='Saturday' OR [functieAngajat]='Friday' OR [functieAngajat]='tester' OR [functieAngajat]='programator' OR [functieAngajat]='manager de proiect' OR [functieAngajat]='director'))
GO
ALTER TABLE [dbo].[Angajati] CHECK CONSTRAINT [functieAngajat]
GO
ALTER TABLE [dbo].[Angajati]  WITH CHECK ADD  CONSTRAINT [IDA] CHECK  (([IDA]>(0) AND [IDA]<=(500)))
GO
ALTER TABLE [dbo].[Angajati] CHECK CONSTRAINT [IDA]
GO
ALTER TABLE [dbo].[Birouri]  WITH CHECK ADD  CONSTRAINT [IDB] CHECK  (([IDB]>(0) AND [IDB]<=(500)))
GO
ALTER TABLE [dbo].[Birouri] CHECK CONSTRAINT [IDB]
GO
ALTER TABLE [dbo].[Domenii]  WITH CHECK ADD  CONSTRAINT [IDD] CHECK  (([IDD]>(0) AND [IDD]<=(500)))
GO
ALTER TABLE [dbo].[Domenii] CHECK CONSTRAINT [IDD]
GO
ALTER TABLE [dbo].[Filiale]  WITH CHECK ADD  CONSTRAINT [IDF] CHECK  (([IDF]>(0) AND [IDF]<=(500)))
GO
ALTER TABLE [dbo].[Filiale] CHECK CONSTRAINT [IDF]
GO
ALTER TABLE [dbo].[Proiecte]  WITH CHECK ADD  CONSTRAINT [IDP] CHECK  (([IDP]>(0) AND [IDP]<=(500)))
GO
ALTER TABLE [dbo].[Proiecte] CHECK CONSTRAINT [IDP]
GO
USE [master]
GO
ALTER DATABASE [Intreprinderi01] SET  READ_WRITE 
GO
