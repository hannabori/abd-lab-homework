use Intreprinderi01;

select * from Angajati;
select * from Proiecte;
select * from Produse;

create table Produse(
	PID bigint PRIMARY KEY,
	IDP int,
	FOREIGN KEY (IDP) references Proiecte(IDP),
	vandut varchar(2),
	constraint check_vandut CHECK (vandut IN ('Da', 'Nu'))
);

insert into Produse VALUES (
411, 401, 'Nu', 'Smart menu'), (412, 402, 'Nu', 'Dating app'), (414, 404, 'Nu', 'MessChat');


alter table Produse
add numeP varchar(50),
foreign key (numeP) references Proiecte(numeP);

alter table Proiecte
add constraint nume_proiect_unic Unique(numeP);

SELECT DATA_TYPE 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE 
     TABLE_NAME = 'Proiecte' AND 
     COLUMN_NAME = 'numeP';

UPDATE Produse
SET vandut = 'Da'
WHERE PID=411;

select * from Produse;

--3.2 De asemenea, sa nu se poata sterge un proiect daca exista angajati 
--care lucreaza la acel proiect. 

ALTER TABLE AngajatiPeProiecte
drop CONSTRAINT FK__AngajatiPeP__IDA__3B75D760, FK__AngajatiPeP__IDP__3A81B327;

ALTER TABLE AngajatiPeProiecte
drop CONSTRAINT IDA_APP_FK;

ALTER TABLE AngajatiPeProiecte
ADD CONSTRAINT IDA_APP_FK
FOREIGN KEY (IDA) REFERENCES Angajati(IDA)
ON DELETE Cascade;

ALTER TABLE AngajatiPeProiecte
ADD CONSTRAINT IDP_APP_FK
FOREIGN KEY (IDP) REFERENCES Proiecte(IDP)
ON DELETE no action;

ALTER TABLE Birouri
drop CONSTRAINT FK__Birouri__IDF__239E4DCF;

ALTER TABLE Angajati
drop CONSTRAINT FK__Angajati__IDB__267ABA7A;

ALTER TABLE Proiecte
drop CONSTRAINT FK__Proiecte__manage__38996AB5;

SELECT * FROM Angajati;

ALTER TABLE Birouri
ADD CONSTRAINT IDF_FK
FOREIGN KEY (IDF) REFERENCES Filiale(IDF)
ON DELETE CASCADE;

ALTER TABLE Angajati
ADD CONSTRAINT IDB_FK
FOREIGN KEY (IDB) REFERENCES Birouri(IDB)
ON DELETE CASCADE;

ALTER TABLE Proiecte
ADD CONSTRAINT IDA_FK
FOREIGN KEY (managerProiect) REFERENCES Angajati(IDA)
ON DELETE no action;

use Intreprinderi01;

SELECT * FROM Filiale;
SELECT * FROM Proiecte;
SELECT * FROM Birouri;
SELECT * FROM Angajati;
SELECT * FROM AngajatiPeProiecte;


delete from Proiecte 
WHERE IDP=402;

insert into Proiecte VALUES 
(408, null, 'Sugar Testing App', '2021-07-15', 'Coca Cola', 8000, 'Erste'),
(409, null, 'LogistikApp', '2021-10-01', 'Dedeman', 1300, 'Raiffeisen Bank');

delete from Proiecte 
WHERE IDP=407;